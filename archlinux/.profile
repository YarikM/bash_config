if [[ -z "$DISPLAY" ]] && [[ $(tty) = /dev/tty1 ]] && [[ -f '/usr/bin/dwm' ]]; then
  startx /usr/bin/dwm
fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        source "$HOME/.bashrc"
    fi
fi
