function bat_lvl {
  if [ -f '/sys/class/power_supply/BAT0/capacity' ]; then
    echo `cat /sys/class/power_supply/BAT0/capacity`%
  else 
    echo ''
  fi
}

export CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
export PS1="\e[94m\t \e[93m`bat_lvl` \e[92m\e[1m\w>\e[0m\e[39m \n# "
export TERM=xterm-256color
export EDITOR=vim
